﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

public class Rover
{
    public static void CalculateRoverPath(int[,] map)
    {
        List<Node> graph = create_graph(map);

        List<Node> reachables = new() { graph.First() };
        List<Node> explored = new();

        Node node_with_best_path = new(0, 0, 0);
        node_with_best_path.cost_path = int.MaxValue;

        while (reachables.Count > 0)
        {
            Node current_node = choose_node(reachables);

            if ((current_node.x == graph.Last().x) && (current_node.y == graph.Last().y))
            {
                // check_best_path_to_current_node
                if(node_with_best_path.cost_path > current_node.cost_path)
                   node_with_best_path = current_node;
            }

            reachables.Remove(current_node);
            explored.Add(current_node);

            List<Node> new_reachable = my_except(graph.Find(fnd => { return (current_node.x == fnd.x) && (current_node.y == fnd.y); }).adjacent_nodes, explored);

            foreach (Node adjacent in new_reachable)
            {
                reachables.Add(adjacent);

                int step_cost = Math.Abs(current_node.weight - adjacent.weight) + 1;
                adjacent.cost_step = step_cost;

                int upd_cost_path = current_node.cost_path + step_cost;

                if (adjacent.cost_path == 0 || upd_cost_path < adjacent.cost_path)
                {
                    adjacent.cost_path = upd_cost_path;
                    adjacent.previous = current_node;
                }
            }
        }
 
        build_path(node_with_best_path);
    }

    private static List<Node> create_graph(int[,] arr)
    {
        // n - height -- y
        // m - widht -- x
        List<Node> graph = new ();

        int height = arr.GetLength(0);
        int width = arr.GetLength(1);

        for (int n = 0; n < height; n++)
        {
            for (int m = 0; m < width; m++)
            {
                Node current = new (n, m, arr[n, m]);
                int up = n - 1;
                int right = m + 1;
                int bottom = n + 1;
                int left = m - 1;

                if (up >= 0)
                    current.adjacent_nodes.Add(new Node(up, m, arr[up,m]));

                if (left >= 0)
                    current.adjacent_nodes.Add(new Node(n, left, arr[n, left]));

                if (bottom < height)
                    current.adjacent_nodes.Add(new Node(bottom, m, arr[bottom, m]));

                if (right < width)
                    current.adjacent_nodes.Add(new Node(n, right, arr[n, right]));

                graph.Add(current);
            }
        }

        return graph;
    }

    private static Node choose_node(List<Node> reachable)
    {
        int min_cost = int.MaxValue;
        Node optimal = default;

        foreach (var node in reachable)
        {
            if (min_cost > node.cost_path)
            {
                min_cost = node.cost_path;
                optimal = node;
            }
        }

        return optimal;
    }
    private static void build_path(Node destination)
    {
        List<Node> rover_path = new();

        while (destination != default)
        {
            rover_path.Add(destination);
            destination = destination.previous;
        }

        rover_path.Reverse();

        string out_str = default;
        int steps = -1;
        int fuel = 0;

        foreach (var move in rover_path)
        {
            if (move != rover_path.Last())
                out_str += "[" + move.y + "][" + move.x + "]->";
            else
                out_str += "[" + move.y + "][" + move.x + "]";

            steps += 1;
            fuel += move.cost_step;
        }

        out_str += "\nsteps: " + steps + "\nfuel: " + fuel;

        string file_path = @".\path-plan.txt";
        using (StreamWriter sw = File.CreateText(file_path))
        { 
            sw.Write(out_str);
        }
    }

    private static List<Node> my_except(List<Node> this_list, List<Node> remove)
    {
        List<Node> result_list = this_list.Where(save => !remove.Any(rem => (rem.x == save.x) && (rem.y == save.y))).ToList();

        return result_list;
    }
}

public class Node
{
    public Node(int y, int x, int weight)
    {
        this.y = y;
        this.x = x;
        this.weight = weight;
    }

    public int x = 0;
    public int y = 0;
    public int weight = 0;
    public int cost_step = 0;
    public int cost_path = 0;

    public Node previous;
    public List<Node> adjacent_nodes = new();
}
